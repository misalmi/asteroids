PLAYER_VIEWPORT_MARGIN = 5

class ViewPort(object):
    def __init__(self, x, y, width, height, world_scale=1.0):
        self.x_window = x
        self.y_window = y
        self.x_world = 0.0
        self.y_world = 0.0
        self.width = width
        self.height = height
        self.world_scale = world_scale

    def update_pos(self, center_x, center_y, world_width, world_height):
        self.x_world = (center_x - self.width / 2) * self.world_scale
        self.y_world = (center_y - self.height / 2) * self.world_scale

def create_for_single_play(window_width, window_height):
    player_viewport = ViewPort(x=0, y=0, width=window_width, height=window_height)
    return [player_viewport]

def _create_for_player1(window_width, window_height):
    middle_width = window_width / 2
    width = middle_width - PLAYER_VIEWPORT_MARGIN
    return ViewPort(x=0, y=0, width=width, height=window_height)

def _create_for_player2(window_width, window_height):
    middle_width = window_width / 2
    x = middle_width + PLAYER_VIEWPORT_MARGIN * 2
    width = middle_width - PLAYER_VIEWPORT_MARGIN
    return ViewPort(x=x, y=0, width=width, height=window_height)

def create_for_two_players(window_width, window_height):
    player1_viewport = _create_for_player1(window_width, window_height)
    player2_viewport = _create_for_player2(window_width, window_height)
    return [player1_viewport, player2_viewport]
