import ships
import objects.base
import objects.ships
import events.ships

ACTIONS = (
    "thrust",
    "drag",
    "rotate_left",
    "rotate_right",
    "fire",
    "respawn",
    "equip1",
    "equip2",
    "equip3",
    "equip4",
)

def perform_actions(dt, actions, players):
    events = []
    for player in players.get_all():
        player_actions = actions[player.name]
        ship_type = ships.SHIP_TYPES[player.ship_type]
        event_cls = ship_type.action_event
        events.append(event_cls(player.name, player_actions, ship_type))
    return events

def _check_keys(action, key_handler, key_conf):
    return any(key_handler[key] for key in key_conf[action])

def _read_local(players, key_handler):
    actions = {}
    for player in players:
        actions[player.name] = []
        for action in ACTIONS:
            if _check_keys(action, key_handler, player.key_conf):
                actions[player.name].append(action)
    return actions

def _read_remote(players):
    actions = {}
    return actions

def _read_npc(world):
    actions = {}
    return actions

def read_new(players, world, key_handler):
    actions = {}
    actions.update(_read_local(players.local_players, key_handler))
    actions.update(_read_remote(players.remote_players))
    actions.update(_read_npc(world))
    return actions
