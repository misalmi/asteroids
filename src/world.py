import math
import random
from collections import namedtuple

import images
import ships
import players as game_players
import events.base
import events.general

WorldState = namedtuple("WorldState", [
    "objects",
    "events",
    "players"
])

def init_next_state(dt, state):
    return WorldState(
        objects=state.objects.copy(),
        events=[],
        players={name : state.copy() for name, state in state.players.items()}
    )

class ObjectsInWorld(object):
    def __init__(self, objects={}, start_ids_from=0):
        self._objects = objects
        self._id_counter = start_ids_from

    def has(self, obj_id):
        return obj_id in self._objects

    def get(self, obj_id):
        return self._objects[obj_id]

    def get_by_type(self, obj_type):
        return [obj for obj in self._objects.values() if obj.type == obj_type]

    def get_all(self):
        return self._objects.values()

    def get_all_ids(self):
        return self._objects.keys()

    def get_all_ids_by_type(self, obj_type):
        return [obj_id for (obj_id, obj) in self._objects.items() if obj.type == obj_type]

    def items(self):
        return self._objects.items()

    def get_id_of_newest(self):
        tries = 1
        while tries > 0:
            id_of_newest = self._id_counter - tries
            if id_of_newest in self._objects:
                return id_of_newest
            else:
                tries += 1
        raise StopIteration("There are no objects in the game world!")

    def add(self, new_object):
        new_id = self._id_counter
        self._id_counter += 1
        self._objects[new_id] = new_object
        return new_id

    def remove(self, obj_id):
        del self._objects[obj_id]

    def copy(self):
        copied_objects = {obj_id : obj.copy() for obj_id, obj in self._objects.items()}
        return ObjectsInWorld(objects=copied_objects, start_ids_from=self._id_counter)

class Settings(object):
    def __init__(self, width, height):
        self.width = width
        self.height = height
        self.asteroid_count = 50
        self.respawn_time = 3
        self.asteroid_split_factor = 5

class GameWorld(object):
    def __init__(self, settings, players, image_args):
        self.width = settings.width
        self.height = settings.height
        self.players = players
        self.image_args = image_args
        self.respawn_time = settings.respawn_time
        self.asteroid_split_factor = settings.asteroid_split_factor

        self.state = WorldState(
            objects=ObjectsInWorld(),
            events=[events.general.InitialEvent(ships.SHIP_TYPES, settings.asteroid_count)],
            players={player.name: game_players.init_player_state(player, settings) for player in players.get_all()}
        )

    def transform_coordinates(self, x, y):
        return x % self.width, y % self.height

    def calculate_coordinate_difference(self, x1, y1, x2, y2):
        dx = x2 - x1
        dy = y2 - y1
        if abs(dx) > self.width / 2:
            dx += math.copysign(self.width, -dx)
        if abs(dy) > self.height / 2:
            dy += math.copysign(self.height, -dy)

        return dx, dy
