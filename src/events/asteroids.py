import random

import events.base
import events.movement as movement

import objects.base

ASTEROID_MAX_ROTATION = 360

class SpawnInitialAsteroidsEvent(events.base.Event):
    ASTEROID_MAX_VELOCITY = 100
    ASTEROID_MAX_ANGULAR_V = 50

    def __init__(self, count, time_elapsed=0):
        super(SpawnInitialAsteroidsEvent, self).__init__(time_elapsed)
        self._count = count

    def unfold(self, world, future_world_state):
        future_objects = future_world_state.objects

        for i in range(self._count):
            asteroid = objects.base.create_object(objects.asteroids.AsteroidObjectDefaultParams, world.image_args)
            if asteroid is None:
                continue
            asteroid.x = random.randrange(0, world.width)
            asteroid.y = random.randrange(0, world.height)
            asteroid.velocity_x = random.randrange(-self.ASTEROID_MAX_VELOCITY, self.ASTEROID_MAX_VELOCITY)
            asteroid.velocity_y = random.randrange(-self.ASTEROID_MAX_VELOCITY, self.ASTEROID_MAX_VELOCITY)
            asteroid.angular_v = random.randrange(-self.ASTEROID_MAX_ANGULAR_V, self.ASTEROID_MAX_ANGULAR_V)
            asteroid.rotation = random.randrange(ASTEROID_MAX_ROTATION)
            future_objects.add(asteroid)

        events = []
        for obj_id in future_objects.get_all_ids_by_type("asteroid"):
            events.append(movement.ObjectMovementEvent(obj_id=obj_id, time_elapsed=0))
        return events

class SpawnReplacementAsteroid(events.base.Event):
    VELOCITY_MAX_DIFF = 50
    ANGULAR_V_MAX_DIFF = 50

    def __init__(self, scale, time_elapsed=0):
        super(SpawnReplacementAsteroid, self).__init__(time_elapsed)
        self._scale = scale

    def unfold(self, world, future_world_state):
        future_objects = future_world_state.objects
        asteroids = [asteroid for asteroid in future_objects.get_by_type("asteroid") if asteroid.scale >= self._scale]
        asteroid = random.choice(asteroids)

        new_asteroid = objects.base.create_object(objects.asteroids.AsteroidObjectDefaultParams, world.image_args)
        if new_asteroid is None:
            return tuple()

        new_asteroid.x = asteroid.x
        new_asteroid.y = asteroid.y
        new_asteroid.velocity_x = asteroid.velocity_x + random.randrange(-self.VELOCITY_MAX_DIFF, self.VELOCITY_MAX_DIFF)
        new_asteroid.velocity_y = asteroid.velocity_y + random.randrange(-self.VELOCITY_MAX_DIFF, self.VELOCITY_MAX_DIFF)
        new_asteroid.rotation = asteroid.rotation
        new_asteroid.angular_v = asteroid.angular_v + random.randrange(-self.ANGULAR_V_MAX_DIFF, self.ANGULAR_V_MAX_DIFF)
        new_asteroid.scale_by(asteroid.scale)
        future_objects.add(new_asteroid)

        obj_id = future_objects.get_id_of_newest()
        return movement.ObjectMovementEvent(obj_id=obj_id, time_elapsed=0),

class SpawnChildAsteroids(events.base.Event):
    VELOCITY_MAX_DIFF = 300
    ANGULAR_V_MAX_DIFF = 200

    def __init__(self, asteroid, time_elapsed=0):
        super(SpawnChildAsteroids, self).__init__(time_elapsed)
        self._asteroid = asteroid

    def unfold(self, world, future_world_state):
        future_objects = future_world_state.objects
        split_factor = world.asteroid_split_factor
        events = []

        for i in range(split_factor):
            new_asteroid = objects.base.create_object(objects.asteroids.AsteroidObjectDefaultParams, world.image_args)
            if new_asteroid is None:
                continue

            max_x_diff = int(self._asteroid.width / 2)
            max_y_diff = int(self._asteroid.height / 2)
            new_asteroid.x = self._asteroid.x + random.randrange(-max_x_diff, max_x_diff)
            new_asteroid.y = self._asteroid.y + random.randrange(-max_y_diff, max_y_diff)
            new_asteroid.velocity_x = self._asteroid.velocity_x + random.randrange(-self.VELOCITY_MAX_DIFF, self.VELOCITY_MAX_DIFF)
            new_asteroid.velocity_y = self._asteroid.velocity_y + random.randrange(-self.VELOCITY_MAX_DIFF, self.VELOCITY_MAX_DIFF)
            new_asteroid.rotation = random.randrange(ASTEROID_MAX_ROTATION)
            new_asteroid.angular_v = self._asteroid.angular_v + random.randrange(self.ANGULAR_V_MAX_DIFF)
            new_asteroid.scale_by(self._asteroid.scale * 2 / split_factor)
            future_objects.add(new_asteroid)

            obj_id = future_objects.get_id_of_newest()
            events.append(movement.ObjectMovementEvent(obj_id=obj_id, time_elapsed=0))
        return events
