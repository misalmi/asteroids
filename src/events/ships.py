import objects.ships as ships
from objects.exceptions import ImageDataMissing
from events.base import Event
from events.weapons import WeaponDelayEvent
from events.general import SpawnPlayerShipEvent
from events.movement import ObjectMovementEvent

class FireActionUpdate(object):
    def __init__(self, actions, player_name, player_state):
        self._actions = actions
        self._player_name = player_name
        self._player_state = player_state
        self._weapon_equipped = player_state.weapon_equipped
        self._weapon_state = player_state.weapon_states[self._weapon_equipped.name]

    def _fire_projectile(self, projectile_id):
        fire_event = self._weapon_equipped.fire_event(self._player_name, projectile_id)
        weapon_delay_event = WeaponDelayEvent(self._player_name, self._weapon_equipped.name)
        movement_event = ObjectMovementEvent(obj_id=projectile_id, time_elapsed=0)
        return (fire_event, weapon_delay_event, movement_event)

    def is_required(self):
        return "fire" in self._actions and self._weapon_state.is_ready()

    def perform(self, ship, future_objects, image_args):
        try:
            projectile = self._weapon_equipped.create_projectile(ship, image_args)
        except ImageDataMissing:
            return tuple()
        else:
            projectile_id = future_objects.add(projectile)
            self._player_state.objects_owned.append(projectile_id)
            self._weapon_state.next_shot()
            return self._fire_projectile(projectile_id)

class AliveShipActionUpdate(object):
    def __init__(self, actions, ship_id, future_objects):
        self._ship_id = ship_id
        self._actions = actions
        self._future_objects = future_objects

    def _update_ship_by_acceleration(self, ship, thrust_factor):
        if "thrust" in self._actions and "drag" in self._actions:
            ship.acceleration = 0
        if "thrust" in self._actions:
            ships.thrust_ship(ship, thrust_factor)
        if "drag" in self._actions:
            ships.drag_ship(ship, thrust_factor)

    def _update_ship_by_rotation(self, ship, rotation_factor):
        if "rotate_left" in self._actions and "rotate_right" in self._actions:
            ship.angular_a = 0
        if "rotate_left" in self._actions:
            ships.rotate_ship_left(ship, rotation_factor)
        if "rotate_right" in self._actions:
            ships.rotate_ship_right(ship, rotation_factor)

    def _update_ship_by_steering_actions(self, ship, effects):
        ships.set_flames_hidden(ship)
        ship.acceleration = 0
        ship.angular_a = 0
        self._update_ship_by_acceleration(ship, effects.thrust_factor)
        self._update_ship_by_rotation(ship, effects.rotation_factor)
        ships.reset_hidden_flames(ship)

    def is_required(self):
        return self._future_objects.has(self._ship_id)

    def perform(self, player_name, player_state, image_args):
        ship = self._future_objects.get(self._ship_id)
        self._update_ship_by_steering_actions(ship, player_state.effects)
        fire_action_update = FireActionUpdate(self._actions, player_name, player_state)
        if fire_action_update.is_required():
            return fire_action_update.perform(ship, self._future_objects, image_args)
        return tuple()

class ShipActionEvent(Event):
    def __init__(self, player_name, actions, ship_type):
        super(ShipActionEvent, self).__init__()
        self._player_name = player_name
        self._actions = actions
        self._ship_type = ship_type

    def _ship_should_respawn(self, ship_id, respawn_time_left):
        respawn_issued = "respawn" in self._actions
        respawn_time_passed = respawn_time_left <= 0
        ship_not_spawned = ship_id is None
        return respawn_issued and respawn_time_passed and ship_not_spawned

    def unfold(self, world, next_world_state):
        future_objects = next_world_state.objects
        player_state = next_world_state.players[self._player_name]
        ship_id = player_state.ship_id

        if self._ship_should_respawn(ship_id, player_state.respawn_time_left):
            return SpawnPlayerShipEvent(self._player_name, self._ship_type),

        ship_action_update = AliveShipActionUpdate(self._actions, ship_id, future_objects)
        if ship_action_update.is_required():
            return ship_action_update.perform(self._player_name, player_state, world.image_args)
        return tuple()
