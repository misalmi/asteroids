import events.base
import objects.explosions

TIME_FADE_OUT = 1.0
OPACITY_DECREMENT_STEP = 5
SCALE_STEPS_N = 15

class RemoveExplosionEvent(events.base.Event):
    def __init__(self, explosion_id, time_elapsed):
        super(RemoveExplosionEvent, self).__init__(time_elapsed)
        self._explosion_id = explosion_id

    def conditions_fulfilled(self, world, future_world_state):
        if self._time_elapsed > TIME_FADE_OUT:
            return True
        else:
            return False

    def unfold(self, world, future_world_state):
        future_objects = future_world_state.objects
        explosion = future_objects.get(self._explosion_id)
        explosion_body = explosion.get_part("body")
        explosion_body.opacity -= OPACITY_DECREMENT_STEP
        if explosion_body.opacity > 0:
            return RemoveExplosionEvent(self._explosion_id, self._time_elapsed),
        else:
            future_world_state.objects.remove(self._explosion_id)
            return tuple()

class ExplosionEvent(events.base.Event):
    def __init__(self, explosion_id, max_scale, time_elapsed):
        super(ExplosionEvent, self).__init__(time_elapsed)
        self._explosion_id = explosion_id
        self._max_scale = max_scale

    def conditions_fulfilled(self, world, future_world_state):
        if self._time_elapsed > 0:
            return True
        else:
            return False

    def unfold(self, world, future_world_state):
        future_objects = future_world_state.objects
        explosion = future_objects.get(self._explosion_id)
        explosion_body_params = objects.base.get_part_params(objects.explosions.ExplosionObjectDefaultParams, "body")
        explosion_body = explosion.get_part("body")
        scale_increment_step = self._max_scale / SCALE_STEPS_N
        explosion_body.scale += scale_increment_step
        if explosion_body.scale >= self._max_scale:
            return RemoveExplosionEvent(self._explosion_id, self._time_elapsed),
        else:
            return ExplosionEvent(self._explosion_id, self._max_scale, self._time_elapsed),
