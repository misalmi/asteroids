import events.base
import objects.base

class ObjectMovementEvent(events.base.Event):
    def __init__(self, obj_id, time_elapsed):
        super(ObjectMovementEvent, self).__init__(time_elapsed)
        self._obj_id = obj_id

    def conditions_fulfilled(self, world, future_world_state):
        if self._time_elapsed > 0:
            return True
        else:
            return False

    def unfold(self, world, future_world_state):
        future_objects = future_world_state.objects
        if future_objects.has(self._obj_id):
            future_objects.get(self._obj_id).move(self._time_elapsed)
            return ObjectMovementEvent(obj_id=self._obj_id, time_elapsed=0),
        else:
            return tuple()
