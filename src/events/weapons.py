import events.base
import events.general
import objects.missiles as missiles

def retrieve_equipped_weapon_data(player_name, future_world_state):
    player_state = future_world_state.players[player_name]
    weapon_equipped = player_state.weapon_equipped
    weapon_state = player_state.weapon_states[weapon_equipped.name]

    return weapon_equipped, weapon_state

class WeaponDelayEvent(events.base.Event):
    def __init__(self, player_name, weapon_name, time_elapsed=0):
        super(WeaponDelayEvent, self).__init__(time_elapsed)
        self._player_name = player_name
        self._weapon_name = weapon_name

    def conditions_fulfilled(self, world, future_world_state):
        weapon_equipped, weapon_state = retrieve_equipped_weapon_data(self._player_name, future_world_state)

        ammo_ready = (weapon_state.ammo_count == weapon_equipped.load_size or
                      self._time_elapsed >= weapon_equipped.ammo_delay)
        reload_ready = (weapon_state.load_count == weapon_equipped.max_load_count or
                        self._time_elapsed >= weapon_equipped.reload_delay)

        if ((not weapon_state.reloaded and reload_ready and ammo_ready) or
            (weapon_state.reloaded and ammo_ready)):
            return True
        else:
            return False

    def unfold(self, world, future_world_state):
        weapon_equipped, weapon_state = retrieve_equipped_weapon_data(self._player_name, future_world_state)
        weapon_state.next_ammo_ready = True
        if not weapon_state.reloaded:
            weapon_state.ammo_count = weapon_equipped.load_size
            weapon_state.load_count -= 1
            weapon_state.reloaded = True

        return tuple()

class MissileEvent(events.base.Event):
    def __init__(self, player_name, missile_id, time_elapsed=0):
        super(MissileEvent, self).__init__()
        self._player_name = player_name
        self._missile_id = missile_id

    def unfold(self, world, future_world_state):
        future_objects = future_world_state.objects
        if not future_objects.has(self._missile_id):
            return tuple()

        missile = future_objects.get(self._missile_id)
        ship_id = future_world_state.players[self._player_name].ship_id
        collision_margin = missile.width
        if (ship_id is None or not
            events.general.collided(missile,
                                         future_world_state.objects.get(ship_id),
                                         world,
                                         margin=collision_margin)):
            missile.parts["body"].collides = True
            missile.collides = True
        missiles.step_flame_animation(missile.parts["flame"])

        return MissileEvent(self._player_name, self._missile_id, time_elapsed=self._time_elapsed),
