import math
import random

import events.base as base
import events.explosions as explosions
import events.asteroids as asteroids
import events.movement as movement

import objects.base
import objects.ships as ships
import objects.asteroids

MAX_COLLISION_DIST = 500
COLLISION_MARGIN = 0

class DestroyObjectEvent(base.Event):
    def __init__(self, obj_id, time_elapsed):
        super(DestroyObjectEvent, self).__init__(time_elapsed)
        self._obj_id = obj_id

    def unfold(self, world, future_world_state):
        future_objects = future_world_state.objects
        explosion = objects.base.create_object(objects.explosions.ExplosionObjectDefaultParams, world.image_args)
        obj = future_objects.get(self._obj_id)
        explosion.x = obj.x
        explosion.y = obj.y
        try:
            explosion_max_scale = obj.width / explosion.width
        except ZeroDivisionError:
            explosion_max_scale = 1.0
        future_objects.remove(self._obj_id)

        events = []
        if obj.type == "asteroid":
            if obj.scale >= objects.asteroids.AsteroidObjectDefaultParams.scale / 4:
                events.append(asteroids.SpawnChildAsteroids(obj))
            if obj.scale >= objects.asteroids.AsteroidObjectDefaultParams.scale:
                events.append(asteroids.SpawnReplacementAsteroid(obj.scale))
        else:
            for player_name, player_state in future_world_state.players.items():
                if self._obj_id == player_state.ship_id:
                    player_state.ship_id = None
                    player_state.respawn_time_left = world.respawn_time
                    events.append(RespawnTimerEvent(player_name))
                if self._obj_id in player_state.objects_owned:
                    player_state.objects_owned.remove(self._obj_id)

        future_objects.add(explosion)
        events.append(explosions.ExplosionEvent(
                explosion_id=future_objects.get_id_of_newest(),
                max_scale = explosion_max_scale,
                time_elapsed=0
        ))
        return events

def calculate_collision_distance(x, y, dx, dy):
    x_dist = abs(dx)
    y_dist = abs(dy)
    return (abs(x) * x_dist + abs(y) * y_dist) / (x_dist + y_dist)

def calculate_collision_distances(obj1, obj2, dx, dy):
    x_min_1, y_min_1, x_max_1, y_max_1 = objects.base.calculate_rotated_object_extremes(obj1)
    x_min_2, y_min_2, x_max_2, y_max_2 = objects.base.calculate_rotated_object_extremes(obj2)

    if dy >= 0 and dx >= 0:
        dist1 = calculate_collision_distance(x_max_1, y_max_1, dx, dy)
        dist2 = calculate_collision_distance(x_min_2, y_min_2, dx, dy)
    elif dy >= 0 and dx < 0:
        dist1 = calculate_collision_distance(x_min_1, y_max_1, dx, dy)
        dist2 = calculate_collision_distance(x_max_2, y_min_2, dx, dy)
    elif dy < 0 and dx >= 0:
        dist1 = calculate_collision_distance(x_max_1, y_min_1, dx, dy)
        dist2 = calculate_collision_distance(x_min_2, y_max_2, dx, dy)
    elif dy < 0 and dx < 0:
        dist1 = calculate_collision_distance(x_min_1, y_min_1, dx, dy)
        dist2 = calculate_collision_distance(x_max_2, y_max_2, dx, dy)

    return dist1, dist2

def collided(obj1, obj2, world, margin=COLLISION_MARGIN):
    if obj1 == obj2:
        return False

    x1, y1 = world.transform_coordinates(obj1.x, obj1.y)
    x2, y2 = world.transform_coordinates(obj2.x, obj2.y)
    dx, dy = world.calculate_coordinate_difference(x1, y1, x2, y2)

    if dx == 0 and dy == 0:
        return True
    elif dx > MAX_COLLISION_DIST or dy > MAX_COLLISION_DIST:
        return False

    collision_dist1, collision_dist2 = calculate_collision_distances(obj1, obj2, dx, dy)
    dist = math.sqrt(dx ** 2 + dy ** 2)
    if dist + COLLISION_MARGIN <= collision_dist1 + collision_dist2:
        return True
    else:
        return False

def calculate_collision_damages_with_object(obj_with_id, others, world):
    obj_id, obj = obj_with_id
    collision_damages = {}
    obj_damage = 0

    for other_id, other in others.items():
        if other.collides and collided(obj, other, world):
            collision_damages[other_id] = obj.damage_inflicts
            obj_damage += other.damage_inflicts

    if obj_damage != 0:
        collision_damages[obj_id] = obj_damage

    return collision_damages

def calculate_updated_damages(old_damages, new_damages):
    collision_damages = old_damages.copy()

    for obj_id, damage in new_damages.items():
        if obj_id in collision_damages:
            collision_damages[obj_id] += damage
        else:
            collision_damages[obj_id] = damage

    return collision_damages

def calculate_new_collision_damages(world, future_world_state):
    collision_damages = {}
    future_objects = future_world_state.objects

    for player_state in future_world_state.players.values():
        for obj_id in player_state.objects_owned:
            obj = future_objects.get(obj_id)
            if not obj.collides:
                continue
            new_damages = calculate_collision_damages_with_object((obj_id, obj), future_objects, world)
            collision_damages = calculate_updated_damages(collision_damages, new_damages)

    return collision_damages

class CollisionDamagesUpdateEvent(base.Event):
    def __init__(self, damages, time_elapsed):
        super(CollisionDamagesUpdateEvent, self).__init__(time_elapsed)
        self._damages = damages

    def unfold(self, world, future_world_state):
        events = []
        future_objects = future_world_state.objects

        new_damages = calculate_new_collision_damages(world, future_world_state)
        total_damages = calculate_updated_damages(self._damages, new_damages)
        for obj_id, damage in total_damages.copy().items():
            if not future_objects.has(obj_id):
                del total_damages[obj_id]
                continue
            obj = future_objects.get(obj_id)
            if damage > obj.damage_to_destroy:
                events.append(DestroyObjectEvent(obj_id=obj_id, time_elapsed=0))
        events.append(CollisionDamagesUpdateEvent(total_damages, self._time_elapsed))

        return events

class RemoteInfluenceEffectsUpdateEvent(base.Event):
    pass

class RespawnTimerEvent(base.Event):
    def __init__(self, player_name, time_elapsed=0):
        super(RespawnTimerEvent, self).__init__(time_elapsed)
        self._player_name = player_name

    def conditions_fulfilled(self, world, future_world_state):
        if self._time_elapsed > 1.0:
            return True
        else:
            return False

    def unfold(self, world, future_world_state):
        player_state = future_world_state.players[self._player_name]
        if player_state.ship_id is not None or player_state.respawn_time_left <= 0:
            return tuple()
        else:
            player_state.respawn_time_left -= 1
            return RespawnTimerEvent(player_name=self._player_name, time_elapsed=0),

class SpawnPlayerShipEvent(base.Event):
    PREFERRED_DISTANCE = 1000
    SHIP_MAX_ROTATION = 360

    def __init__(self, player_name, ship_type, time_elapsed=0):
        super(SpawnPlayerShipEvent, self).__init__(time_elapsed)
        self._player_name = player_name
        self._ship_type = ship_type

    def unfold(self, world, future_world_state):
        try:
            player = world.players.get_player(self._player_name)
        except StopIteration:
            return tuple()

        dist = self.PREFERRED_DISTANCE
        future_objects = future_world_state.objects
        player_ship = objects.base.create_object(self._ship_type.obj_params, world.image_args)

        while True:
            ship_x = random.randrange(0, world.width)
            ship_y = random.randrange(0, world.height)
            for obj in future_objects.get_all():
                obj_x, obj_y = world.transform_coordinates(obj.x, obj.y)
                dx, dy = world.calculate_coordinate_difference(ship_x, ship_y, obj_x, obj_y)
                if math.sqrt(dx ** 2 + dy ** 2) < dist:
                    break
            else:
                break
            dist -= 1

        player_ship.x = ship_x
        player_ship.y = ship_y
        player_ship.rotation = random.randrange(self.SHIP_MAX_ROTATION)
        future_objects.add(player_ship)

        player_state = future_world_state.players[self._player_name]
        ship_id = future_objects.get_id_of_newest()
        player_state.ship_id = ship_id
        player_state.objects_owned.append(ship_id)

        return movement.ObjectMovementEvent(obj_id=ship_id, time_elapsed=0),

class InitialEvent(base.Event):
    def __init__(self, ship_types, asteroid_count, time_elapsed=0):
        super(InitialEvent, self).__init__(time_elapsed)
        self._ship_types = ship_types
        self._asteroid_count = asteroid_count

    def unfold(self, world, future_world_state):
        events = [asteroids.SpawnInitialAsteroidsEvent(
            count=self._asteroid_count
        )]
        for player in world.players.get_all():
            ship_type = self._ship_types[player.ship_type]
            events.append(SpawnPlayerShipEvent(player.name, ship_type, time_elapsed=0))
        events.append(CollisionDamagesUpdateEvent(damages={}, time_elapsed=0))
        return events
