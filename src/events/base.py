import events

class Event(object):
    def __init__(self, time_elapsed=0):
        self._time_elapsed = time_elapsed

    def conditions_fulfilled(self, world, future_world_state):
        return True

    def unfold(self, world, future_world_state):
        raise NotImplementedError("This method must be implemented!")

    def step(self, dt):
        self._time_elapsed += dt
