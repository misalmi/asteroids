import math

import events
import world as game_world
import actions as game_actions
import objects.base

class GameLogic(object):
    def __init__(self, settings):
        self._settings = settings

    def _carry_out_events(self, dt, world, next_world_state):
        next_events = next_world_state.events
        for event in world.state.events:
            event.step(dt)
            if event.conditions_fulfilled(world, next_world_state):
                new_events = event.unfold(world, next_world_state)
                next_events.extend(new_events)
            else:
                next_events.append(event)

    def update_world_state(self, dt, actions, world):
        events = game_actions.perform_actions(dt, actions, world.players)
        next_world_state = game_world.init_next_state(dt, world.state)
        next_world_state.events.extend(events)
        self._carry_out_events(dt, world, next_world_state)
        world.state = next_world_state

    def update_hud(self, world):
        pass
