import math

import objects.base as obj_base
import objects.exceptions as exceptions
import objects.missiles as missiles
import events.weapons as weapons

class Weapon(object):
    name = ""
    load_size = 1
    initial_load_count = 1
    max_load_count = 1
    reload_delay = 0
    ammo_delay = 0
    fire_event = None

    @staticmethod
    def create_projectile(ship, image_args):
        raise NotImplementedError("This method must be implemented!")

class MissileWeapon(Weapon):
    name = "missile"
    load_size = 3
    initial_load_count = 10
    max_load_count = 10
    reload_delay = 1
    ammo_delay = 1
    fire_event = weapons.MissileEvent

    @staticmethod
    def create_projectile(ship, image_args):
        missile = obj_base.create_object(missiles.MissileObjectDefaultParams, image_args)
        if missile is None:
            raise exceptions.ImageDataMissing("Missing missile from weapon image data!")

        velocity = missile.velocity_x
        rotation_rad = math.radians(ship.rotation)
        rotation_cos = math.cos(rotation_rad)
        rotation_sin = math.sin(rotation_rad)
        missile.x = ship.x + ship.width * rotation_cos
        missile.y = ship.y + ship.width * -rotation_sin
        missile.rotation = ship.rotation
        missile.velocity_x = velocity * rotation_cos
        missile.velocity_y = velocity * -rotation_sin
        missile.velocity_x += ship.velocity_x
        missile.velocity_y += ship.velocity_y
        missile.scale_by(ship.scale)

        return missile

class EquipmentState(object):
    def __init__(self, equipment):
        self.equipment = equipment
        self.load_count = equipment.initial_load_count
        self.ammo_count = equipment.load_size
        self.next_ammo_ready = True
        self.reloaded = True

    def copy(self):
        equip_state = EquipmentState(self.equipment)
        equip_state.load_count = self.load_count
        equip_state.ammo_count = self.ammo_count
        equip_state.next_ammo_ready = self.next_ammo_ready
        equip_state.reloaded = self.reloaded

    def is_ready(self):
        return self.ammo_count > 0 and self.next_ammo_ready

    def next_shot(self):
        self.next_ammo_ready = False
        if self.ammo_count == 1:
            self.reloaded = False
        elif self.ammo_count > 0:
            self.ammo_count -= 1

def init_equipment_states(equipment):
    return {equipped.name : EquipmentState(equipped) for equipped in equipment}
