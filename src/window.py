import math
import random

import pyglet
from pyglet.gl import *

import images
import objects.base

SAFE_TO_DESTROY_DIST = 100

def point_x_relation_to_viewport(x, viewport, world_width):
    x_relation = 0
    x = x % world_width

    world_crossed_hor = False

    x_min_viewport = viewport.x_world % world_width
    x_max_viewport = ((x_min_viewport + viewport.width) * viewport.world_scale) % world_width
    if x_min_viewport > x_max_viewport:
        world_crossed_hor = True

    if not world_crossed_hor:
        if x < x_min_viewport and x_min_viewport - x <= world_width - x_max_viewport + x:
            x_relation = -1
        elif x >= x_max_viewport and x - x_max_viewport < x_min_viewport + world_width - x:
            x_relation = 1
        elif x < x_min_viewport:
            x_relation = 1
        elif x > x_max_viewport:
            x_relation = -1
    elif x < x_min_viewport and x >= x_max_viewport:
        if x_min_viewport - x <= x - x_max_viewport:
            x_relation = -1
        else:
            x_relation = 1
    return x_relation

def point_y_relation_to_viewport(y, viewport, world_height):
    y_relation = 0
    y = y % world_height

    world_crossed_ver = False

    y_min_viewport = viewport.y_world % world_height
    y_max_viewport = ((y_min_viewport + viewport.height) * viewport.world_scale) % world_height
    if y_min_viewport > y_max_viewport:
        world_crossed_ver = True

    if not world_crossed_ver:
        if y < y_min_viewport and y_min_viewport - y <= world_height - y_max_viewport + y:
            y_relation = -1
        elif y >= y_max_viewport and y - y_max_viewport < y_min_viewport + world_height - y:
            y_relation = 1
        elif y < y_min_viewport:
            y_relation = 1
        elif y >= y_max_viewport:
            y_relation = -1
    elif y < y_min_viewport and y >= y_max_viewport:
        if y_min_viewport - y <= y - y_max_viewport:
            y_relation = -1
        else:
            y_relation = 1
    return y_relation

def rect_in_viewport(x_min, y_min, x_max, y_max, viewport, world_dimensions):
    world_width, world_height = world_dimensions
    x_rect_min = x_min % world_width
    y_rect_min = y_min % world_height
    x_rect_max = x_max % world_width
    y_rect_max = y_max % world_height

    world_crossed_hor = False
    world_crossed_ver = False

    x_min_viewport = viewport.x_world % world_width
    x_max_viewport = ((x_min_viewport + viewport.width) * viewport.world_scale) % world_width
    if x_min_viewport > x_max_viewport:
        world_crossed_hor = True

    y_min_viewport = viewport.y_world % world_height
    y_max_viewport = ((y_min_viewport + viewport.height) * viewport.world_scale) % world_height
    if y_min_viewport > y_max_viewport:
        world_crossed_ver = True

    if not world_crossed_hor:
        if x_rect_min > x_rect_max and x_rect_max <= x_min_viewport and x_rect_min > x_max_viewport:
            return False
        elif x_rect_min <= x_rect_max and (x_rect_max <= x_min_viewport or x_rect_min > x_max_viewport):
            return False
    elif x_rect_max >= x_rect_min and x_rect_max <= x_min_viewport and x_rect_min > x_max_viewport:
        return False

    if not world_crossed_ver:
        if y_rect_min > y_rect_max and y_rect_max <= y_min_viewport and y_rect_min > y_max_viewport:
            return False
        elif y_rect_min <= y_rect_max and (y_rect_max <= y_min_viewport or y_rect_min > y_max_viewport):
            return False
    elif y_rect_max >= y_rect_min and y_rect_max <= y_min_viewport and y_rect_min > y_max_viewport:
        return False

    return True

def obj_in_viewport(obj, viewport, world_dimensions):
    safe_dist = SAFE_TO_DESTROY_DIST * viewport.world_scale
    x_min, y_min, x_max, y_max = objects.base.calculate_rotated_object_extremes(obj)
    x_port_min = obj.x + x_min - safe_dist
    y_port_min = obj.y + y_min - safe_dist
    x_port_max = obj.x + x_max + safe_dist
    y_port_max = obj.y + y_max + safe_dist
    return rect_in_viewport(x_port_min, y_port_min, x_port_max, y_port_max, viewport, world_dimensions)

class GameWindowSettings(object):
    def __init__(self, width, height):
        self.width = width
        self.height = height
        self.fullscreen = False
        self.fps_limit = 60

def create_background_sprites(viewport, batch):
    image_name = "stars"
    try:
        #https://en.wikipedia.org/wiki/Hyades_(star_cluster)#/media/File:Hyades.jpg
        bg_image = images.load_image(image_name)
    except pyglet.resource.ResourceNotFoundException:
        print("Image {name}.png not found in path!".format(name=image_name))
        return

    view_middle_x= viewport.width / 2
    view_middle_y= viewport.height / 2
    positions = [(view_middle_x, view_middle_y)]
    if viewport.width >= viewport.height:
        scale = viewport.width / bg_image.width
        rotation = 0
        bg_width = bg_image.width * scale
        bg_height = bg_image.height * scale
        positions.extend(((view_middle_x - bg_width, view_middle_y),
                          (view_middle_x, view_middle_y - bg_height),
                          (view_middle_x - bg_width, view_middle_y - bg_height)))

    else:
        scale = viewport.width / bg_image.height
        rotation = 90
        bg_width = bg_image.width * scale
        bg_height = bg_image.height * scale
        positions.extend(((view_middle_x - bg_height, view_middle_y),
                          (view_middle_x, view_middle_y + bg_width),
                          (view_middle_x - bg_height, view_middle_y + bg_width)))

    bg_dict = {}
    for pos_x, pos_y in positions:
        sprite = pyglet.sprite.Sprite(img=bg_image)
        sprite.scale = scale
        sprite.x = pos_x
        sprite.y = pos_y
        sprite.rotation = rotation
        sprite.batch = batch
        sprite.group = pyglet.graphics.OrderedGroup(0)
        bg_dict[(pos_x, pos_y)] = sprite
    return bg_dict

def calc_sprite_x(obj_part, obj, viewport, world_width):
    dx_obj_viewport = (obj.x % world_width - viewport.x_world % world_width) % viewport.width
    offset = viewport.width * point_x_relation_to_viewport(obj.x, viewport, world_width)
    x_obj_viewport = dx_obj_viewport + offset
    x_relative_trans = obj_part.relative_x * math.cos(math.radians(obj.rotation))  + (obj_part.relative_y * math.sin(math.radians(obj.rotation)))
    return (x_obj_viewport + x_relative_trans) * viewport.world_scale

def calc_sprite_y(obj_part, obj, viewport, world_height):
    dy_obj_viewport = (obj.y % world_height - viewport.y_world % world_height) % viewport.height
    offset = viewport.height * point_y_relation_to_viewport(obj.y, viewport, world_height)
    y_obj_viewport = viewport.y_window + dy_obj_viewport + offset
    y_relative_trans = -obj_part.relative_x * math.sin(math.radians(obj.rotation)) + (obj_part.relative_y * math.cos(math.radians(obj.rotation)))
    return (y_obj_viewport + y_relative_trans) * viewport.world_scale

def create_sprite(obj_part, obj, batch, image, viewport, world_dimensions):
    world_width, world_height = world_dimensions
    sprite = pyglet.sprite.Sprite(img=image,
                                  x=calc_sprite_x(obj_part, obj, viewport, world_width),
                                  y=calc_sprite_y(obj_part, obj, viewport, world_height))
    sprite.rotation = obj.rotation + obj_part.relative_rot
    sprite.scale = obj_part.scale * viewport.world_scale
    sprite.visible = obj_part.visible
    sprite.opacity = obj_part.opacity
    sprite.group = pyglet.graphics.OrderedGroup(obj_part.z_index)
    sprite.batch = batch
    return sprite

def update_sprite(sprite, obj_part, obj, viewport, world_dimensions):
    # Cropping in case of multiple viewports?
    world_width, world_height = world_dimensions
    sprite.x = calc_sprite_x(obj_part, obj, viewport, world_width)
    sprite.y = calc_sprite_y(obj_part, obj, viewport, world_height)
    sprite.rotation = obj.rotation + obj_part.relative_rot
    sprite.scale = obj_part.scale * viewport.world_scale
    sprite.visible = obj_part.visible
    sprite.opacity = obj_part.opacity

def update_viewport_coordinates(viewports, world):
    objects = world.state.objects
    for player in world.players.get_all():
        viewport = viewports[player.viewport_id]
        ship_id = world.state.players[player.name].ship_id
        if objects.has(ship_id):
            ship = objects.get(ship_id)
            viewport.update_pos(ship.x, ship.y, world.width, world.height)

def update_viewport_background(viewport, bg_dict, old_viewport_coords):
    old_x, old_y = old_viewport_coords
    dx = (old_x - viewport.x_world) * viewport.world_scale
    dy = (old_y - viewport.y_world) * viewport.world_scale

    for (pos_x, pos_y), bg_sprite in bg_dict.items():
        bg_sprite.x += dx
        bg_sprite.y += dy

        if viewport.width >= viewport.height:
            bg_width = bg_sprite.width
            bg_height = bg_sprite.height
        else:
            bg_width = bg_sprite.height
            bg_height = bg_sprite.width

        if bg_sprite.x >= viewport.x_window + viewport.width:
            offset = bg_sprite.x - viewport.width
            bg_sprite.x = viewport.x_window - bg_width + offset
        elif bg_sprite.x + bg_sprite.width <= viewport.x_window:
            offset = bg_sprite.x + bg_width
            bg_sprite.x = viewport.x_window + viewport.width + offset

        if bg_sprite.y >= viewport.y_window + viewport.height:
            offset = bg_sprite.y - viewport.height
            bg_sprite.y = viewport.y_window - bg_height + offset
        elif bg_sprite.y + bg_sprite.height <= viewport.y_window:
            offset = bg_sprite.y + bg_height
            bg_sprite.y = viewport.y_window + viewport.height + offset

        # How to handle rotated viewports?

def update_backgrounds(viewports, bg_dict, old_viewport_coords):
    for i, viewport in enumerate(viewports):
       if bg_dict[i] is not None:
           update_viewport_background(viewport, bg_dict[i], old_viewport_coords[i])

def update_viewport_sprites(viewport, sprite_dict, batch, images, world):
    world_dimensions = (world.width, world.height)

    # Initialize a list of object ids which should be removed from the sprite dict, based on the current ids in the dict
    to_remove_obj_ids = list(sprite_dict.keys())

    for obj_id, obj in world.state.objects.items():
        if obj_id not in sprite_dict:
            sprite_dict[obj_id] = {}
        else:
            # The object exists, so it is removed from the list of object ids which should be removed from the sprite dict
            to_remove_obj_ids.remove(obj_id)
        obj_sprites = sprite_dict[obj_id]

        # Delete sprites which have moved outside the viewport
        if not obj_in_viewport(obj, viewport, world_dimensions):
            [sprite.delete() for sprite in obj_sprites.values()]
            sprite_dict[obj_id] = {}
            continue

        # Create sprites, which do not yet exist but should. Update already existing ones
        for obj_part_id, obj_part in obj.parts.items():
            if obj_part_id not in obj_sprites:
                try:
                    image = images[obj_part.image_name]
                except KeyError:
                    print("Sprite creation failed since {}.png was missing!".format(obj_part.image_name))
                else:
                    obj_sprites[obj_part_id] = create_sprite(obj_part, obj, batch, image, viewport, world_dimensions)
            else:
                update_sprite(obj_sprites[obj_part_id], obj_part, obj, viewport, world_dimensions)

    # Remove the ids of the objects, which do not exist anymore, from the sprite dict
    for obj_id in to_remove_obj_ids:
        del sprite_dict[obj_id]

def update_sprites(viewports, sprite_dict, batches, images, world):
    for i, viewport in enumerate(viewports):
        try:
            viewport_sprites = sprite_dict[i]
        except KeyError:
            viewport_sprites = {}
            sprite_dict[i] = viewport_sprites
        for viewport_id in sprite_dict.keys():
            if viewport_id >= len(viewports):
                del sprite_dict[viewport_id]
        update_viewport_sprites(viewport, viewport_sprites, batches[i], images, world)

def update_scoreboards(viewports, players):
    pass

class GameWindow(pyglet.window.Window):
    """Main window of the Asteroids game."""

    def __init__(self, settings, viewports, key_handler):
        super(GameWindow, self).__init__(settings.width, settings.height)
        self._viewports = viewports
        self._sprite_dict = {}
        self._bg_dict = {}
        self._batches = [pyglet.graphics.Batch() for viewport in viewports]
        for i, viewport in enumerate(viewports):
            self._sprite_dict[i] = {}
            self._bg_dict[i] = create_background_sprites(viewport, self._batches[i])

        #self._hiscore = ...
        self.set_fullscreen(settings.fullscreen)
        self.push_handlers(key_handler)
        #pyglet.clock.set_fps_limit(settings.fps_limit)

    def on_draw(self):
        self.clear()

        for i, viewport in enumerate(self._viewports):
            x = int(viewport.x_window)
            y = int(viewport.y_window)
            width = int(viewport.width)
            height = int(viewport.height)

            glViewport(x, y, width, height)
            glMatrixMode(gl.GL_PROJECTION)
            glLoadIdentity()
            glOrtho(0, width, 0, height, -1, 1)
            glMatrixMode(gl.GL_MODELVIEW)

            self._batches[i].draw()

    def on_close(self):
        pass

    def update(self, images, world):
        old_viewport_coords = [(viewport.x_world, viewport.y_world) for viewport in self._viewports]
        update_viewport_coordinates(self._viewports, world)
        update_backgrounds(self._viewports, self._bg_dict, old_viewport_coords)
        update_sprites(self._viewports, self._sprite_dict, self._batches, images, world)
        update_scoreboards(self._viewports, world.players)
