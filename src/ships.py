from collections import namedtuple

import objects.ships as ship_objects
import events.ships as ship_events

ShipType = namedtuple("ShipType", [
    "action_event",
    "obj_params",
])

SHIP_TYPES = {
    "default_ship" : ShipType(
        action_event=ship_events.ShipActionEvent,
        obj_params=ship_objects.ShipObjectDefaultParams
    ),
}
