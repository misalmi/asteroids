from collections import namedtuple

import equipment

PLAYER_ARGS = [
    "name",
    "ship_type",
    "initial_weapon_choices",
    "initial_boost_choices",
]

PlayerEffects = namedtuple("PlayerEffects", [
    "thrust_factor",
    "drag_factor",
    "rotation_factor",
])

LocalPlayer = namedtuple("LocalPlayer", PLAYER_ARGS + [
    "viewport_id",
    "key_conf",
])

RemotePlayer = namedtuple("RemotePlayer", PLAYER_ARGS + [
    "viewport_id",
    "key_conf",
])

NpcPlayer = namedtuple("NpcPlayer", PLAYER_ARGS + [
    "npc_ai",
])

class PlayerState(object):
    def __init__(self, score, ship_id, objects_owned, weapon_equipped, boost_equipped, weapon_states, boost_states, effects, respawn_time_left):
        self.score = score
        self.ship_id = ship_id
        self.objects_owned = objects_owned
        self.weapon_equipped = weapon_equipped
        self.boost_equipped = boost_equipped
        self.weapon_states = weapon_states
        self.boost_states = boost_states
        self.effects = effects
        self.respawn_time_left = respawn_time_left

    def copy(self):
        return PlayerState(
            self.score,
            self.ship_id,
            self.objects_owned[:],
            self.weapon_equipped,
            self.boost_equipped,
            self.weapon_states.copy(),
            self.boost_states.copy(),
            self.effects,
            self.respawn_time_left
        )

def init_player_state(player, world_settings):
    return PlayerState(
        score=0,
        ship_id=None,
        objects_owned=[],
        weapon_equipped=player.initial_weapon_choices[0],
        boost_equipped=player.initial_boost_choices[0],
        weapon_states=equipment.init_equipment_states(player.initial_weapon_choices),
        boost_states=equipment.init_equipment_states(player.initial_boost_choices),
        effects=PlayerEffects(thrust_factor=1, drag_factor=1, rotation_factor=1),
        respawn_time_left=world_settings.respawn_time
    )

class Players(object):
    def __init__(self, local_players, remote_players=[], npc_players=[]):
        self.local_players = local_players
        self.remote_players = remote_players
        self.npc_players = npc_players

    def get_all(self):
        return self.local_players + self.remote_players + self.npc_players

    def get_player(self, name):
        for player in self.get_all():
            if player.name == name:
                return player
        raise StopIteration("No player with name {} found!".format(name))
