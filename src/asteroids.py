import argparse

import pyglet

import world
import logic
import window
import images
import events
import equipment
import players as game_players
import actions as game_actions
import viewports as game_viewports

UPDATE_INTERVAL = 120.0
SAVEFILE = "savefile.dat"
WINDOW_WIDTH = 1920
WINDOW_HEIGHT = 1080

WORLD_SCALE_FACTOR = 5
WORLD_WIDTH = WORLD_SCALE_FACTOR * WINDOW_WIDTH
WORLD_HEIGHT = WORLD_SCALE_FACTOR * WINDOW_HEIGHT

def create_single_player():
    player = game_players.LocalPlayer(
        name="Yky-Pertteli",
        ship_type="default_ship",
        initial_weapon_choices=[equipment.MissileWeapon],
        initial_boost_choices=[equipment.MissileWeapon],
        viewport_id=0,
        key_conf={
            "thrust" : (pyglet.window.key.UP,),
            "drag" :  (pyglet.window.key.DOWN,),
            "rotate_left" : (pyglet.window.key.LEFT,),
            "rotate_right" : (pyglet.window.key.RIGHT,),
            "fire" : (pyglet.window.key.SPACE,),
            "respawn" : (pyglet.window.key.R,),
            "equip1" : (pyglet.window.key.NUM_1,),
            "equip2" : (pyglet.window.key.NUM_2,),
            "equip3" : (pyglet.window.key.NUM_3,),
            "equip4" : (pyglet.window.key.NUM_4,),
        })
    return game_players.Players([player])

def create_player1():
    return game_players.LocalPlayer(
        name="Yky-Pertteli",
        ship_type="default_ship",
        initial_weapon_choices=[equipment.MissileWeapon],
        initial_boost_choices=[equipment.MissileWeapon],
        viewport_id=0,
        key_conf={
            "thrust" : (pyglet.window.key.UP,),
            "drag" :  (pyglet.window.key.DOWN,),
            "rotate_left" : (pyglet.window.key.LEFT,),
            "rotate_right" : (pyglet.window.key.RIGHT,),
            "fire" : (pyglet.window.key.SPACE,),
            "respawn" : (pyglet.window.key.PLUS,),
            "equip1" : (pyglet.window.key.NUM_6,),
            "equip2" : (pyglet.window.key.NUM_7,),
            "equip3" : (pyglet.window.key.NUM_8,),
            "equip4" : (pyglet.window.key.NUM_9,),
        })

def create_player2():
    return game_players.LocalPlayer(
        name="Yky-Pertteli2",
        ship_type="default_ship",
        initial_weapon_choices=[equipment.MissileWeapon],
        initial_boost_choices=[equipment.MissileWeapon],
        viewport_id=1,
        key_conf={
            "thrust" : (pyglet.window.key.W,),
            "drag" :  (pyglet.window.key.S,),
            "rotate_left" : (pyglet.window.key.A,),
            "rotate_right" : (pyglet.window.key.D,),
            "fire" : (pyglet.window.key.LCTRL,),
            "respawn" : (pyglet.window.key.R,),
            "equip1" : (pyglet.window.key.NUM_1,),
            "equip2" : (pyglet.window.key.NUM_2,),
            "equip3" : (pyglet.window.key.NUM_3,),
            "equip4" : (pyglet.window.key.NUM_4,),
        })

def create_two_players():
    player1 = create_player1()
    player2 = create_player2()
    return game_players.Players([player1, player2])

class AsteroidsGame(object):
    def __init__(self, player_count):
        if player_count == 1:
            viewports = game_viewports.create_for_single_play(WINDOW_WIDTH, WINDOW_HEIGHT)
            players = create_single_player()
        else:
            viewports = game_viewports.create_for_two_players(WINDOW_WIDTH, WINDOW_HEIGHT)
            players = create_two_players()

        self._key_handler = pyglet.window.key.KeyStateHandler()
        window_settings = window.GameWindowSettings(WINDOW_WIDTH, WINDOW_HEIGHT)
        self._window = window.GameWindow(window_settings, viewports, self._key_handler)

        self._images = images.load_images()
        image_data = images.create_image_params_by_name(self._images)
        world_settings = world.Settings(WORLD_WIDTH, WORLD_HEIGHT)
        self._world = world.GameWorld(world_settings, players, image_data)
        self._logic = logic.GameLogic(settings={})

    def update(self, dt):
        players = self._world.players
        actions = game_actions.read_new(players, world, self._key_handler)
        self._logic.update_world_state(dt, actions, self._world)
        self._logic.update_hud(self._world)
        self._window.update(self._images, self._world)

def run_app(args):
    game = AsteroidsGame(player_count=args.player_count)
    pyglet.clock.schedule_interval(game.update, 1 / UPDATE_INTERVAL)
    try:
        pyglet.app.run()
    except KeyboardInterrupt as e:
        pyglet.app.exit()

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--players", dest="player_count", help="The number players in the game", type=int, default=1)
    args = parser.parse_args()
    run_app(args)
