import os

import pyglet

RESOURCE_PATH = "../resources"

# https://openclipart.org/detail/38155/nasa-space-shuttle
# https://en.wikipedia.org/wiki/21_Lutetia#/media/File:Rosetta_triumphs_at_asteroid_Lutetia.jpg
# https://pixabay.com/p-38187/?no_redirect
# https://pixabay.com/en/fireball-explosion-devastation-422748/
# https://openclipart.org/detail/82435/missile
IMAGE_NAMES = (
    "ship",
    "asteroid",
    "flame",
    "explosion",
    "missile"
)

def reindex_resource_dir(path):
    pyglet.resource.path = [path]
    pyglet.resource.reindex()

def center_image(image):
    image.anchor_x = image.width / 2
    image.anchor_y = image.height / 2

def load_image(name, centered=False):
    reindex_resource_dir(RESOURCE_PATH)
    image = pyglet.resource.image(name + ".png")
    if centered:
        center_image(image)
    return image

def load_images():
    images = {}
    reindex_resource_dir(RESOURCE_PATH)
    for name in IMAGE_NAMES:
        try:
            # TODO: Do not assume centered anchor
            images[name] = load_image(name, centered=True)
        except pyglet.resource.ResourceNotFoundException:
            print("Image {name}.png not found in path {path}!".format(name=name, path=RESOURCE_PATH))
    return images

class ImageParams(object):
    def __init__(self, name, width, height, anchor_x, anchor_y):
        self.name = name
        self.width = width
        self.height = height
        self.anchor_x = anchor_x
        self.anchor_y = anchor_y

def create_single_image_params(name, image):
    return ImageParams(name, image.width, image.height, image.anchor_x, image.anchor_y)

def create_image_params_by_name(images):
    return {name : create_single_image_params(name, image)
            for name, image in images.items()}
