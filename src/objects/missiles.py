import objects.base as base
import objects.animations as animations

MissileObjectDefaultParams = base.ObjectParams(
    parts={
        "body" : base.PartParams(
            image_name="missile",
            scale=1.0,
            relative_x=0.0,
            relative_y=0.0,
            relative_rot=0.0,
            z_index=1,
            visible=True,
            opacity=255,
            collides=False,
        ),
        "flame" : base.PartParams(
            image_name="flame",
            scale=1.0,
            relative_x=-98.0,
            relative_y=1.0,
            relative_rot=0.0,
            z_index=1,
            visible=True,
            opacity=255,
            collides=False,
        ),
    },
    type="missile",
    x=0.0,
    y=0.0,
    rotation=0.0,
    scale=1.0,
    velocity_x=1000.0,
    velocity_y=0.0,
    acceleration=1000.0,
    angular_v=0.0,
    angular_a=0.0,
    collides=False,
    damage_to_destroy=1,
    damage_inflicts=100,
)

MissileFlameAnimationParams = animations.FlameAnimationParams(step=0.03, oscillation=5)

def get_flame_params():
    return MissileObjectDefaultParams.parts["flame"]

def step_flame_animation(flame):
    defaults = get_flame_params()
    animations.step_flame_animation(flame, defaults, MissileFlameAnimationParams)
