import objects.base

AsteroidObjectDefaultParams = objects.base.ObjectParams(
    parts={
        "body" : objects.base.PartParams(
            image_name="asteroid",
            scale=1.0,
            relative_x=0.0,
            relative_y=0.0,
            relative_rot=0.0,
            z_index=3,
            visible=True,
            opacity=255,
            collides=True,
        ),
    },
    type="asteroid",
    x=0.0,
    y=0.0,
    rotation=0.0,
    scale=0.9,
    velocity_x=0.0,
    velocity_y=0.0,
    acceleration=0.0,
    angular_v=0.0,
    angular_a=0.0,
    collides=True,
    damage_to_destroy=1,
    damage_inflicts=100,
)
