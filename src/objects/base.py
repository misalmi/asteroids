import math
from collections import namedtuple

import images

PART_KWARGS_KEYS = (
    "scale",
    "relative_x",
    "relative_y",
    "relative_rot",
    "z_index",
    "visible",
    "opacity",
    "collides",
)
PartParams = namedtuple("PartParams", ("image_name",) + PART_KWARGS_KEYS)

OBJECT_KWARGS_KEYS = (
    "type",
    "x",
    "y",
    "rotation",
    "scale",
    "velocity_x",
    "velocity_y",
    "acceleration",
    "angular_v",
    "angular_a",
    "collides",
    "damage_to_destroy",
    "damage_inflicts",
)
ObjectParams = namedtuple("ObjectParams", ("parts",) + OBJECT_KWARGS_KEYS)

def get_part_params(obj_params, part_name):
    return obj_params.parts[part_name]

class ObjectPart(object):
    def __init__(self, image_data, **kwargs):
        self.image_name = image_data.name
        self.width = image_data.width
        self.height = image_data.height
        self.anchor_x = image_data.anchor_x
        self.anchor_y = image_data.anchor_y

        for attr, val in kwargs.items():
            setattr(self, attr, val)

def calculate_position(dt, x0, y0, v_x0, v_y0, a, rotation):
    x = x0 + v_x0 * dt + (0.5 * a * dt ** 2) * math.cos(math.radians(rotation))
    y = y0 + v_y0 * dt + (0.5 * a * dt ** 2) * -math.sin(math.radians(rotation))
    return x, y

def calculate_velocity(dt, v_x0, v_y0, a, rotation):
    v_x = v_x0 + a * dt * math.cos(math.radians(rotation))
    v_y = v_y0 + a * dt * -math.sin(math.radians(rotation))
    return v_x, v_y

def calculate_rotation(dt, rot0, angular_v0, angular_a):
    return rot0 + angular_v0 * dt + angular_a * dt ** 2

def calculate_angular_v(dt, angular_v0, angular_a):
    return angular_v0 + angular_a * dt

def calculate_rotated_extremes(x_min, y_min, x_max, y_max, rot):
    rads = math.radians(rot)
    cos = math.cos(rads)
    sin = math.sin(rads)
    x_coords = (x_min * cos, y_min * sin, x_max * cos, y_max * sin)
    y_coords = (x_min * sin, y_min * cos, x_max * sin, y_max * cos)
    return min(x_coords), min(y_coords), max(x_coords), max(y_coords)

def calculate_extremes_of_rotated_part(part):
    x_min = -part.anchor_x * part.scale
    y_min = -part.anchor_y * part.scale
    x_max = (part.width - part.anchor_x) * part.scale
    y_max = (part.height - part.anchor_y) * part.scale
    x_rot_min, y_rot_min, x_rot_max, y_rot_max = calculate_rotated_extremes(x_min, y_min, x_max, y_max, part.relative_rot)
    x_abs_min = x_rot_min + part.relative_x
    y_abs_min = y_rot_min + part.relative_y
    x_abs_max = x_rot_max + part.relative_x
    y_abs_max = y_rot_max + part.relative_y
    return x_abs_min, y_abs_min, x_abs_max, y_abs_max

def calculate_extremes_of_object_parts(parts):
    x_min, y_min = 0, 0
    x_max, y_max = 0, 0
    for part_name, part in parts.items():
        if not part.visible:
            continue
        x_part_min, y_part_min, x_part_max, y_part_max = calculate_extremes_of_rotated_part(part)
        if x_part_min < x_min:
            x_min = x_part_min
        if y_part_min < y_min:
            y_min = y_part_min
        if x_part_max > x_max:
            x_max = x_part_max
        if y_part_max > y_max:
            y_max = y_part_max

    return x_min, y_min, x_max, y_max

def calculate_dimensions_of_object_parts(parts):
    x_min, y_min, x_max, y_max = calculate_extremes_of_object_parts(parts)
    width = x_max - x_min
    height = y_max - y_min

    return width, height

def calculate_rotated_object_extremes(obj):
    x_parts_min, y_parts_min, x_parts_max, y_parts_max = calculate_extremes_of_object_parts(obj.parts)
    return calculate_rotated_extremes(x_parts_min, y_parts_min, x_parts_max, y_parts_max, obj.rotation)

class GameObject(object):
    def __init__(self, parts, do_scale=True, **kwargs):
        self.parts = parts

        for attr, val in kwargs.items():
            setattr(self, attr, val)
        if do_scale:
            self._scale_parts(self.scale)
        self.width, self.height = calculate_dimensions_of_object_parts(self.parts)

    def get_part(self, part_name):
        return self.parts[part_name]

    def _scale_parts(self, factor):
        for part in self.parts.values():
            part.scale *= factor
            part.relative_x *= factor
            part.relative_y *= factor

    def scale_by(self, factor):
        self.scale *= factor
        self._scale_parts(factor)
        self.width, self.height = calculate_dimensions_of_object_parts(self.parts)

    def move(self, dt):
        self.x, self.y = calculate_position(dt, self.x, self.y, self.velocity_x, self.velocity_y, self.acceleration, self.rotation)
        self.velocity_x, self.velocity_y = calculate_velocity(dt, self.velocity_x, self.velocity_y, self.acceleration, self.rotation)
        self.rotation = calculate_rotation(dt, self.rotation, self.angular_v, self.angular_a)
        self.angular_v = calculate_angular_v(dt, self.angular_v, self.angular_a)

    def copy(self):
        obj_parts = {}
        for part_name, part in self.parts.items():
            image_data = images.ImageParams(part.image_name, part.width, part.height, part.anchor_x, part.anchor_y)
            part_kwargs = {kwarg_name : getattr(part, kwarg_name) for kwarg_name in PART_KWARGS_KEYS}
            obj_parts[part_name] =  ObjectPart(image_data=image_data, **part_kwargs)

        obj_kwargs = {kwarg_name : getattr(self, kwarg_name) for kwarg_name in OBJECT_KWARGS_KEYS}
        return GameObject(
            parts=obj_parts,
            do_scale=False,
            **obj_kwargs
        )

def create_object(obj_args, image_args):
    parts = {}
    for part_name, (image_name, *part_args) in obj_args.parts.items():
        try:
            single_img_data = image_args[image_name]
        except KeyError:
            print("Object creation failed since {}.png was missing!".format(image_name))
            return
        else:
            part_kwargs = {kwarg_name : part_args[i] for i, kwarg_name in enumerate(PART_KWARGS_KEYS)}
            parts[part_name] = ObjectPart(single_img_data, **part_kwargs)

    obj_kwargs = {kwarg_name : obj_args[1:][i] for i, kwarg_name in enumerate(OBJECT_KWARGS_KEYS)}
    obj = GameObject(parts, **obj_kwargs)
    return obj
