from collections import namedtuple

FlameAnimationParams = namedtuple("ShipFlameAnimationParams", ["step", "oscillation"])

DIR_LEFT = 0
DIR_RIGHT = 180
DIR_UP = -90
DIR_DOWN = 90

def scale_flame(flame, scale, default_params):
    max_scale = default_params.scale
    default_relative_x, default_relative_y = default_params.relative_x, default_params.relative_y
    default_rotation = default_params.relative_rot

    # TODO: Make this whole scaling smarter
    flame.scale = scale
    if default_rotation == DIR_LEFT:
        flame.relative_x = default_relative_x + flame.anchor_x * (max_scale - flame.scale)
    elif default_rotation == DIR_RIGHT:
        flame.relative_x = default_relative_x - flame.anchor_x * (max_scale - flame.scale)
    elif default_rotation == DIR_UP:
        flame.relative_y = default_relative_y + flame.anchor_x * (max_scale - flame.scale)
    elif default_rotation == DIR_DOWN:
        flame.relative_y = default_relative_y - flame.anchor_x * (max_scale - flame.scale)

def step_flame_animation(flame, default_params, animation_params):
    max_scale = default_params.scale

    if flame.scale < max_scale:
        new_scale = flame.scale + animation_params.step
    else:
        new_scale = max_scale - animation_params.step * animation_params.oscillation
    scale_flame(flame, new_scale, default_params)
