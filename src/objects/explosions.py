import objects.base

ExplosionObjectDefaultParams = objects.base.ObjectParams(
    parts = {
        "body" : objects.base.PartParams(
            image_name="explosion",
            scale=0.5,
            relative_x=0.0,
            relative_y=0.0,
            relative_rot=0.0,
            z_index=3,
            visible=True,
            opacity=255,
            collides=False,
        ),
    },
    type="explosion",
    x=0.0,
    y=0.0,
    rotation=0.0,
    scale=1.0,
    velocity_x=0.0,
    velocity_y=0.0,
    acceleration=0.0,
    angular_v=0.0,
    angular_a=0.0,
    collides=False,
    damage_to_destroy=1,
    damage_inflicts=0,
)
