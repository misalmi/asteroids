import objects.base as base
import objects.animations as animations

ShipObjectDefaultParams = base.ObjectParams(
    parts={
        "body" : base.PartParams(
            image_name="ship",
            scale=1.0,
            relative_x=0.0,
            relative_y=0.0,
            relative_rot=0.0,
            z_index=2,
            visible=True,
            opacity=255,
            collides=True,
        ),
        "front_flame_left" : base.PartParams(
            image_name="flame",
            scale=0.8,
            relative_x=35.0,
            relative_y=36.0,
            relative_rot=180.0,
            z_index=1,
            visible=False,
            opacity=255,
            collides=False,
        ),
        "front_flame_right" : base.PartParams(
            image_name="flame",
            scale=0.8,
            relative_x=35.0,
            relative_y=-38.0,
            relative_rot=180.0,
            z_index=1,
            visible=False,
            opacity=255,
            collides=False,
        ),
        "rear_flame_left" : base.PartParams(
            image_name="flame",
            scale=0.8,
            relative_x=-140.0,
            relative_y=10.0,
            relative_rot=0.0,
            z_index=1,
            visible=False,
            opacity=255,
            collides=False,
        ),
        "rear_flame_right" : base.PartParams(
            image_name="flame",
            scale=0.8,
            relative_x=-140.0,
            relative_y=-12.0,
            relative_rot=0.0,
            z_index=1,
            visible=False,
            opacity=255,
            collides=False,
        ),
        "frontside_flame_left" : base.PartParams(
            image_name="flame",
            scale=0.5,
            relative_x=75.0,
            relative_y=42.0,
            relative_rot=90.0,
            z_index=1,
            visible=False,
            opacity=255,
            collides=False,
        ),
        "frontside_flame_right" : base.PartParams(
            image_name="flame",
            scale=0.5,
            relative_x=75.0,
            relative_y=-42.0,
            relative_rot=-90.0,
            z_index=1,
            visible=False,
            opacity=255,
            collides=False,
        ),
        "rearside_flame_left" : base.PartParams(
            image_name="flame",
            scale=0.5,
            relative_x=-65.0,
            relative_y=100.0,
            relative_rot=90.0,
            z_index=3,
            visible=False,
            opacity=255,
            collides=False,
        ),
        "rearside_flame_right" : base.PartParams(
            image_name="flame",
            scale=0.5,
            relative_x=-65.0,
            relative_y=-100.0,
            relative_rot=-90.0,
            z_index=3,
            visible=False,
            opacity=255,
            collides=False,
        ),
    },
    type="ship",
    x=100.0,
    y=100.0,
    rotation=0.0,
    scale=0.6,
    velocity_x=0.0,
    velocity_y=0.0,
    acceleration=0.0,
    angular_v=0.0,
    angular_a=0.0,
    collides=True,
    damage_to_destroy=1,
    damage_inflicts=1,
)

SHIP_TYPE = {"default_ship" : ShipObjectDefaultParams}

ShipFlameAnimationParams = animations.FlameAnimationParams(step=0.02, oscillation=5)

FLAME_ANIM_START_SCALE = 0.1
THRUST = 500
DRAG = -500
ROTATE = 150

def get_flame_part_names():
    return [flame_name for flame_name, _ in ShipObjectDefaultParams.parts.items() if "flame" in flame_name]

def get_flame_params():
    return [ShipObjectDefaultParams.parts[flame_name] for flame_name in get_flame_part_names()]

def step_flame_animation(flame, part_name, scale):
    defaults = ShipObjectDefaultParams.parts[part_name]
    defaults = base.PartParams(
        image_name=defaults.image_name,
        scale=defaults.scale * scale,
        relative_x=defaults.relative_x * scale,
        relative_y=defaults.relative_y * scale,
        relative_rot=defaults.relative_rot,
        z_index=defaults.z_index,
        visible=defaults.visible,
        opacity=defaults.opacity,
        collides=defaults.collides,
    )
    animations.step_flame_animation(flame, defaults, ShipFlameAnimationParams)

def step_front_flame_anim(flame_left, flame_right, scale):
    step_flame_animation(flame_left, "front_flame_left", scale)
    step_flame_animation(flame_right, "front_flame_right", scale)

def step_rear_flame_anim(flame_left, flame_right, scale):
    step_flame_animation(flame_left, "rear_flame_left", scale)
    step_flame_animation(flame_right, "rear_flame_right", scale)

def step_left_rotating_flame_anim(flame_left, flame_right, scale):
    step_flame_animation(flame_left, "rearside_flame_left", scale)
    step_flame_animation(flame_right, "frontside_flame_right", scale)

def step_right_rotating_flame_anim(flame_left, flame_right, scale):
    step_flame_animation(flame_left, "frontside_flame_left", scale)
    step_flame_animation(flame_right, "rearside_flame_right", scale)

def thrust_ship(ship, force):
    ship.acceleration = THRUST * force
    rear_flame_left = ship.get_part("rear_flame_left")
    rear_flame_right = ship.get_part("rear_flame_right")
    rear_flame_left.visible = True
    rear_flame_right.visible = True
    step_rear_flame_anim(rear_flame_left, rear_flame_right, ship.scale)

def drag_ship(ship, force):
    ship.acceleration = DRAG * force
    front_flame_left = ship.get_part("front_flame_left")
    front_flame_right = ship.get_part("front_flame_right")
    front_flame_left.visible = True
    front_flame_right.visible = True
    step_front_flame_anim(front_flame_left, front_flame_right, ship.scale)

def rotate_ship_left(ship, force):
    ship.angular_a = -ROTATE * force
    rearside_flame_left = ship.get_part("rearside_flame_left")
    frontside_flame_right = ship.get_part("frontside_flame_right")
    rearside_flame_left.visible = True
    frontside_flame_right.visible = True
    step_left_rotating_flame_anim(rearside_flame_left, frontside_flame_right, ship.scale)

def rotate_ship_right(ship, force):
    ship.angular_a = ROTATE * force
    frontside_flame_left = ship.get_part("frontside_flame_left")
    rearside_flame_right = ship.get_part("rearside_flame_right")
    frontside_flame_left.visible = True
    rearside_flame_right.visible = True
    step_right_rotating_flame_anim(frontside_flame_left, rearside_flame_right, ship.scale)

def retrieve_ship_flames(ship):
    return [ship.get_part(flame_name) for flame_name in get_flame_part_names()]

def set_flames_hidden(ship):
    for flame in retrieve_ship_flames(ship):
        flame.visible = False

def reset_hidden_flames(ship):
    flames = retrieve_ship_flames(ship)
    flame_defaults = get_flame_params()
    for flame, flame_defaults in zip(flames, flame_defaults):
        if not flame.visible:
            animations.scale_flame(flame, FLAME_ANIM_START_SCALE, flame_defaults)
